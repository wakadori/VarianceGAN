#coding:utf-8
# http://qiita.com/mattya/items/e5bfe5e04b9d2f0bbd47
""" DCGANの2層バージョン(2018/07/17作成) """
import sys

import chainer
import chainer.functions as F
import chainer.links as L

sys.path.append('./net')
import elu


class Generator(chainer.Chain):
    def __init__(self, nz):
        super(Generator, self).__init__(
            l0 = L.Linear(nz, 12*12*128),
            dc1 = L.Deconvolution2D(128, 64, 4, stride=2, pad=1, use_cudnn=False),
            dc2 = L.Deconvolution2D(64, 3, 4, stride=2, pad=1, use_cudnn=False),
            bnl0 = L.BatchNormalization(12*12*128),
            bndc1 = L.BatchNormalization(64),
        )
        
    def __call__(self, z, test=False):
        h = F.reshape(F.relu(self.bnl0(self.l0(z), test=test), use_cudnn=False),
                      (z.data.shape[0], 128, 12, 12))
        h = F.relu(self.bndc1(self.dc1(h), test=test), use_cudnn=False)
        x = F.tanh(self.dc2(h), use_cudnn=False)
        return x


class Discriminator(chainer.Chain):
    def __init__(self):
        super(Discriminator, self).__init__(
            c0 = L.Convolution2D(3, 64, 4, stride=2, pad=1, use_cudnn=False),
            c1 = L.Convolution2D(64, 128, 4, stride=2, pad=1, use_cudnn=False),
            l2 = L.Linear(12*12*128, 2),
            bnc1 = L.BatchNormalization(128),
        )
        
    def __call__(self, x, test=False):
        h = elu.elu(self.c0(x))
        h = elu.elu(self.bnc1(self.c1(h), test=test))
        l = self.l2(h)
        return l
