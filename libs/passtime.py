#coding:utf-8
"""
時間計測を行うプログラム
初期化時にpasstimeインスタンスを作る(保存先time.txtの作成とスタート時間を測定)
displayを呼ぶとその時の経過時間などを表示(infに追加表示情報も渡せる,同時にtime.txtに保存)
"""
from datetime import datetime

class passtime:
    def __init__(self, savePath):
        self.savePath = savePath
        self.stime = datetime.now()
        with open('./{}/time.txt'.format(self.savePath), 'w') as f:
            pass

    def display(self, inf=''):
        etime = datetime.now() #時間計測とlogへの保存
        len_time = etime - self.stime
        second_time = int(etime.strftime('%s')) - int(self.stime.strftime('%s'))
        log = 'start time {}\nnow   time {}\ntime {}\nsecond {}[s]'.format(self.stime, etime, len_time, second_time)
        if inf is not '':
            log += '\n{}'.format(inf)
        print(log)
        with open('./{}/time.txt'.format(self.savePath), 'a') as f:
            f.write(log+'\n')
