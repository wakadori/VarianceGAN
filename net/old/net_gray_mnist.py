# coding:utf-8
"""
DCGAN(グレースケール版, MNIST用)
       入力層   中間層    出力層
 -Gen :nz(整数), 14x14x32, 28x28x1
 -Disc:28x28x1, 14x14x32, 2
"""
import chainer
import chainer.functions as F
import chainer.links as L

class Generator(chainer.Chain):
    def __init__(self, nz):
        super(Generator, self).__init__(
            in_linear = L.Linear(nz, 14*14*32),
            out_deconv = L.Deconvolution2D(32, 1, 4, stride=2, pad=1),
        )
    def __call__(self, z):
        batchsize = z.data.shape[0];
        h = F.reshape(F.relu(self.in_linear(z)), (batchsize, 32, 14, 14))
        x = (self.out_deconv(h))
        return x

class Discriminator(chainer.Chain):
    def __init__(self):
        super(Discriminator, self).__init__(
            in_conv = L.Convolution2D(1, 32, 4, stride=2, pad=1),
            out_linear = L.Linear(14*14*32, 2),
        )
    def __call__(self, x):
        h = F.relu(self.in_conv(x))
        h = self.out_linear(h)
        return h
