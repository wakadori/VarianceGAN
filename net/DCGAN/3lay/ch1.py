#coding:utf-8
# http://qiita.com/mattya/items/e5bfe5e04b9d2f0bbd47
""" DCGANの3層バージョン(2018/07/18作成) """
import sys

import chainer
import chainer.functions as F
import chainer.links as L

sys.path.append('./net')
import elu


class Generator(chainer.Chain):
    def __init__(self, nz):
        super(Generator, self).__init__(
            l0 = L.Linear(nz, 6*6*256),
            dc1 = L.Deconvolution2D(256, 128, 4, stride=2, pad=1, use_cudnn=False),
            dc2 = L.Deconvolution2D(128, 64, 4, stride=2, pad=1, use_cudnn=False),
            dc3 = L.Deconvolution2D(64, 3, 4, stride=2, pad=1, use_cudnn=False),
            bnl0 = L.BatchNormalization(6*6*256),
            bndc1 = L.BatchNormalization(128),
            bndc2 = L.BatchNormalization(64),
        )
        
    def __call__(self, z, test=False):
        h = F.reshape(F.relu(self.bnl0(self.l0(z), test=test), use_cudnn=False),
                      (z.data.shape[0], 256, 6, 6))
        h = F.relu(self.bndc1(self.dc1(h), test=test), use_cudnn=False)
        h = F.relu(self.bndc2(self.dc2(h), test=test), use_cudnn=False)
        x = F.tanh(self.dc3(h), use_cudnn=False)
        return x


class Discriminator(chainer.Chain):
    def __init__(self):
        super(Discriminator, self).__init__(
            c0 = L.Convolution2D(3, 64, 4, stride=2, pad=1, use_cudnn=False),
            c1 = L.Convolution2D(64, 128, 4, stride=2, pad=1, use_cudnn=False),
            c2 = L.Convolution2D(128, 256, 4, stride=2, pad=1, use_cudnn=False),
            l3 = L.Linear(6*6*256, 2),
            bnc1 = L.BatchNormalization(128),
            bnc2 = L.BatchNormalization(256),
        )
        
    def __call__(self, x, test=False):
        h = elu.elu(self.c0(x))
        h = elu.elu(self.bnc1(self.c1(h), test=test))
        h = elu.elu(self.bnc2(self.c2(h), test=test))
        l = self.l3(h)
        return l
