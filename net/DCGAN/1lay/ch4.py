#coding:utf-8
# http://qiita.com/mattya/items/e5bfe5e04b9d2f0bbd47
""" DCGANの1層バージョン """
import sys

import chainer
import chainer.functions as F
import chainer.links as L

sys.path.append('./net')
import elu


class Generator(chainer.Chain):
    def __init__(self, nz):
        super(Generator, self).__init__(
            l0z = L.Linear(nz, 24*24*64)
            dc1 = L.Deconvolution2D(64, 3, 4, stride=2, pad=1),
            bn0l = L.BatchNormalization(24*24*64),
        )
        
    def __call__(self, z, test=False):
        with chainer.using_config('test', test):
            h = F.reshape(F.relu(self.bn0l(self.l0z(z))),
                          (z.data.shape[0], 64, 24, 24))
            x = F.tanh(self.dc1(h))
            return x


class Discriminator(chainer.Chain):
    def __init__(self):
        super(Discriminator, self).__init__(
            c0 = L.Convolution2D(3, 64, 4, stride=2, pad=1),
            l4l = L.Linear(24*24*64, 2),
        )
        
    def __call__(self, x, test=False):
        with chainer.using_config('test', test):
            h = elu.elu(self.c0(x))
            l = self.l4l(h)
            return l
