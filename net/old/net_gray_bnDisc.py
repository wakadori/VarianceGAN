# coding:utf-8
"""
DCGAN(グレースケール版)
       入力層         中間層    出力層
 -Gen :2,             12x12x32, 24x24x1
 -Disc:24x24x1, bn32, 12x12x32, 2
"""
import chainer
import chainer.functions as F
import chainer.links as L

class Generator(chainer.Chain):
    def __init__(self, nz):
        super(Generator, self).__init__(
            in_linear = L.Linear(nz, 12*12*32),
            out_deconv = L.Deconvolution2D(32, 1, 4, stride=2, pad=1),
        )
    def __call__(self, z):
        batchsize = z.data.shape[0];
        h = F.relu(F.reshape(self.in_linear(z), (batchsize, 32, 12, 12)))
        h = self.out_deconv(h)
        return h

class Discriminator(chainer.Chain):
    def __init__(self):
        super(Discriminator, self).__init__(
            in_conv = L.Convolution2D(1, 32, 4, stride=2, pad=1),
            out_linear = L.Linear(12*12*32, 2),
            bn0 = L.BatchNormalization(32),
        )
    def __call__(self, x):
        h = F.relu(self.bn0(self.in_conv(x)))
        h = self.out_linear(h)
        return h
