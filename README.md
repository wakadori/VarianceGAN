## VarianceGAN
My code of GAN that I use in my research. Data is not involved.  
これは研究で使っているコードです。データと出力は含めていません。  
python2.7.6とchainer1.11.0環境、python3.5.2とchainer4.2.0環境のどちらでも動きます。

### 使い方/Usage
#### 1回の実験を行う
1. 学習を実行する。  
`$python train.py --dataPath /path/to/data/directory`
2. 詳しくはヘルプ参照。  
`$python train.py -h`

#### 複数回の実験を行う
1. train.sh内のデータパスを変更する。  
`DATA=("/path/to/data/dir1" "/path/to/data/dir2" "/path/to/data/dir3" ...)  # データ3つ以上の例`
2. train.sh内のパラメータalphaを設定する。  
`ALPHA=(0.0 0.1 0.4 0.7 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0)  # alphaが14パターンの例`
3. 学習を実行する。  
`$bash train.sh`