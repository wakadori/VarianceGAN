#coding:utf-8
# chainer.__version__ '1.11.0'
# http://qiita.com/mattya/items/e5bfe5e04b9d2f0bbd47
"""
GANの学習済みモデルから画像を生成するプログラム改良版
使い方例:
figure_gen = FigureGenerator(model, nz, ...)  # 図作成器の初期化
fig = figure_gen(params)  # 図の作成
fig.savefig(...)  # 保存
"""
import numpy as np
import chainer.functions as F
from chainer import Variable, optimizers, serializers, cuda
from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import plt_flatdesign
plt_flatdesign.main()


class ImageGenerator(object):
    """
    ある1epochでのGANモデル設定と画像の生成を担う．
    コールするたびに指定枚数の画像リストを返す．
    [input]
     -model   (module): 使用するGANのモデル
     -param   (str)   : 使用するパラメータのパス
     -nz      (int)   : 潜在変数zの次元
     -gpu     (int)   : 使用するGPUID
     -iscolor (bool)  : カラー画像フラグ(逆はグレスケ)
    """
    def __init__(self, model, param, nz, gpu, iscolor):
        """ モデルの初期化 """
        self.model = model
        self.param = param
        self.nz = nz
        self.gpu = gpu
        self.iscolor = iscolor

        self.model_gen = self.model.Generator(self.nz)  # Genモデル作成
        if self.gpu >= 0:                               # GPU設定
            cuda.get_device(self.gpu).use()
            self.model_gen.to_gpu()
        #xp = np if self.gpu < 0 else cuda.cupy
        serializers.load_npz(param, self.model_gen)     # パラメータのロード

    def __call__(self, *args, **kw):
        """ 画像を生成して返す． """
        return self.__gen_images(*args, **kw)

    def __clipping(self, x):
        """
        生成画像の画素値をクリッピング(0~1外の値を無理やり収める)
        [input]
         - x (scalar): ある画素値の値．np.vectorize()での使用を想定
        """
        if x < 0:
            x = 0
        if x > 1:
            x = 1
        return np.float32(x)

    def __set_z_length(self, length):
        """ self.__gen_zで使うlengthをセットする(事前にしておかないとエラー) """
        self.length = length

    def __gen_z(self):
        """
        乱数列を生成して返す．
        [output]
         - z (Variable): 乱数列
        """
        z = Variable(cuda.cupy.random.uniform(
            -1, 1, (self.length, self.nz), dtype=np.float32))
        return z

    def __gen_images(self, length=1):
        """
        画像を生成して返す
        [input]
         - length  (int)          : 生成する枚数
        [output]
         - x_trans (list of Image): 画像列
        """
        self.__set_z_length(length)  # 生成個数セット
        z = self.__gen_z()  # 乱数生成
        x_gpu = self.model_gen(z).data  # 画像生成
        x = cuda.to_cpu(x_gpu)  # numpyのメソッドを使うための変換(cupy->numpy)
        del z, x_gpu

        # 画像への変換
        x_trans = []  # 変換した画像
        for i in range(np.shape(x)[0]):
            image = np.vectorize(self.__clipping)(x[i, :, :, :])  # クリッピング
            if self.iscolor is True:  # カラーの場合
                image = np.uint8(image.transpose(1, 2, 0)*255)  # CHWをHWCに変換
            else:  # グレーの場合
                image = np.uint8(image*255)  # グレスケに戻す
                image = image[0, :, :]  # 無意味な二重括弧を消す？
            image = Image.fromarray(image)  # Imageオブジェクトに変換
            x_trans.append(image)
            del image
        return x_trans


class FigureGenerator(object):
    """
    画像をプロットした図の作成を担う．コールするたびに指定したパラメータで生成を行い図にして返す．
    [input]
     -model   (module): 使用するGANのモデル
     -nz      (int)   : 潜在変数zの次元
     -gpu     (int)   : 使用するGPUID
     -iscolor (bool)  : カラー画像フラグ(逆はグレスケ)
    """
    def __init__(self, model, nz, gpu, iscolor):
        """ モデルの初期化 """
        self.model = model
        self.nz = nz
        self.gpu = gpu
        self.iscolor = iscolor

    def __call__(self, *args, **kw):
        return self.__gen_epochs_fig(*args, **kw)

    def __gen_epochs_fig(self, params, rows=1, shape=[10, 10]):
        """
        複数epochでの図を作成して返す
        [input]
         - params (list of str): 使用するパラメータのパス列
                                 これがepoch指定を兼ねる
         - rows   (int)        : プロットするepochの行数
         - shape  (pair of int): 1epoch内でのプロットの形
        [output]
         - fig    (Figure)     : プロット語の図
        """
        if isinstance(params, str):  # パスが1つの時リスト化
            params = [params]

        # 初期化
        columns = int(np.ceil(len(params)/rows))  # 列数
        if columns * rows < len(params):
            rows += 1  # 行数
        fig = plt.figure(num=None,  # 全体の図
                         figsize=(int(shape[1]*columns*0.7),
                                  int(shape[0]*rows*0.7)), frameon=True)
        #if self.iscolor is False:
        #    plt.style.use('grayscale')
        gs = gridspec.GridSpec(rows, columns)  # epoch毎の図
        gs_geo = gs.get_geometry()  # 縦横取得

        # プロット
        for r in range(gs_geo[0]):  # epochを順番に見る
            for c in range(gs_geo[1]):
                subgs = gridspec.GridSpecFromSubplotSpec(  # 1epoch内の図
                    shape[0], shape[1], subplot_spec=gs[r, c])
                subgs_geo = subgs.get_geometry()  # 縦横取得
                if r*gs_geo[1] + c >= len(params):  # paramsの範囲外で終了
                    break
                gen = ImageGenerator(  # 生成器作成
                    self.model, params[r*gs_geo[1] + c],
                    self.nz, self.gpu, self.iscolor)
                x_trans = gen(length=int(subgs_geo[0] * subgs_geo[1]))  # 画像生成
                epoch = params[r*gs_geo[1] + c].split(  # epoch取得
                    'gen_e')[-1].replace('.model', '')
                fig = self.__plot_images(
                    x_trans, fig, GridSpec=subgs, epoch=epoch)  # プロット
                del gen, x_trans  # 解放
        return fig

    def __plot_images(self, x_trans, fig,
                      GridSpec=gridspec.GridSpec(1, 1), epoch='epoch'):
        """
        受け取ったFigureに画像を格子状プロットして返す
        [input]
         - x_trans (list of Image)          : Imageオブジェクトに変換した画像列
         - fig     (Figure)                 : プロット対象のFigureオブジェクト
         - GridSpec(GridSpecFromSubplotSpec): fig中でsubplotする範囲
         - epoch   (str)                    : epochプロット用
        [output]
         - fig     (Figure)                 : プロット語の図
        """
        size = x_trans[0].size  # 画像の横縦の長さ(枠線プロットに使う)
        space = 0.2  # 枠線余白
        gs = GridSpec  # 命名短縮
        gs_geo = gs.get_geometry()  # 縦横取得
        for r in range(gs_geo[0]):  # 画像を一枚ずつプロット
            for c in range(gs_geo[1]):
                ax = plt.Subplot(fig, gs[r, c])
                ax.imshow(x_trans[r*gs_geo[1] + c])  # 画像表示
                ax.set_axis_off()  # 軸の数字を消す

                # 枠線プロット
                if r == 0:  # 上枠線
                    ax.text(int(-size[0]*space/2),
                            int(-size[1]*space/2), '-----------------')
                if r == gs_geo[0] - 1:  # 下枠線
                    ax.text(int(-size[0]*space/2),
                            int(size[1]*(1+space)), '-----------------')
                    if c == 0:  # epochプロット
                        ax.text(0,
                                int(size[1]*(1+space)*1.5),
                                'epoch {}'.format(epoch), fontsize=20)
                if c == 0:  # 左枠線
                    ax.text(int(-size[0]*space), int(size[1]),
                            '|\n|\n|\n|\n|')
                if c == gs_geo[1] - 1:  # 右枠線
                    ax.text(int(size[0]*(1+space/2)), int(size[1]),
                            '|\n|\n|\n|\n|')
                fig.add_subplot(ax)
                del ax
        return fig
