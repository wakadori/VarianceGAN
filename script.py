#coding:utf-8
""" train.pyを繰り返し実行する """

import argparse
import json
import os
import pickle
import skopt
import sys
import traceback
import warnings
from datetime import datetime as dt
from skopt import gp_minimize  # ベイズ最適化

from pdb import set_trace as tr

# GAN本体
import train

# 自作ライブラリ
sys.path.append('./libs')
#import BayesianPlot as blt
import PlotLib as plt  # lossなどのプロット

# 保存場所
output = 'output/{}bayesian'.format(dt.now().strftime('%m%d'))
if os.path.isdir(output) is False:
    os.makedirs(output)

""" コマンドライン引数読み込み """
parser = argparse.ArgumentParser(description='train.pyを繰り返し実行する')
parser.add_argument('--epochsize', '-e', default=10000, type=int, help='学習回数')
parser.add_argument('--saveTimes', '-t', default=10, type=int, help='保存回数')
parser.add_argument('--alpha', '-a', default=0., type=float,
                    help='分散項の最大値．デフォルトは0(すなわち分散項無し)．')
parser.add_argument('--learningrate', '-r', default=0.0001,
                    type=float, help='学習率')
parser.add_argument('--batchsize', '-b', default=240, type=int, help='バッチサイズ')
parser.add_argument('--dimz', '-nz', default=100, type=int, help='zの次元数')
parser.add_argument('--savePath', default=None,
                    help='既存のディレクトリを指定して続きを学習する時に使う．')
parser.add_argument('--gpu', '-g', default=0, type=int,
                    help='GPU ID (負の値ならCPUモード)')
parser.add_argument('--netparams', default="{'model':'DCGAN', 'layer':4}",
                    help='使うモデルを指定する．dict形式．')

parser.add_argument('--dataPaths', default=['../input/muririn_R',
                                            '../input/tsunako_R',
                                            '../input/kantoku_R'],
                    nargs='+',  # 1個以上の指定
                    help='データセットディレクトリのリスト')
parser.add_argument('--seeds', '-s', default=[17350159,
                                              797039838,
                                              12915411],
                    type=int, nargs='*',  # 0個以上の指定
                    help='シード指定用リスト．' \
                    'seeds[0]がdataPaths[0]のシードとなる．Noneは指定なし．')
parser.add_argument('--n_calls', default=20, type=int,
                    help='ベイズ最適化 サンプル数．')
parser.add_argument('--n_random_starts', default=5, type=int,
                    help='ベイズ最適化 ランダムサンプル数．初回だけ5回にする．')
parser.add_argument('--init_points', default=None, type=str,
                    help='ベイズ最適化 初期化データのパス．pickleファイルまたは.gzファイルかデータの揃ったディレクトリを指定する．Noneは指定なし．')
args = parser.parse_args()

""" 引数の前処理 """
# シード
if len(args.seeds) == 0:  # 指定なし
    args.seeds = [None]*len(args.dataPaths)
elif len(args.seeds) == len(args.dataPaths):  # 指定あり
    args.seeds = [int(x) for x in args.seeds]
else:  # それ以外のフォーマットはエラー
    try:
        raise ValueError('SeedsLengthError!')
    except ValueError as e:
        print(e)

# train.pyに渡す引数を設定
i = 1
args.__setattr__('dataPath', args.dataPaths[i])  # 試験的にデータとシードを固定
args.__setattr__('seed', args.seeds[i])

def func(inputs, args=args):
    """
    ベイズ最適化用(train.pyのデコレータ)．ある条件のargsに対して返り値を返す．
    [input]
     inputs (tuple): ベイズ最適化の入力となる値．
     args  (argparse.Namespace): inputs以外が設定された引数．
    [output]
     ret (float): train.pyの返り値をそのまま返す．ここでは最終epochでのtest_variance
    """
    args.alpha, args.learningrate = inputs  # 引数を渡す
    args.savePath = '{}/{}'.format(output, dt.now().strftime('%m%d_%H_%M_%S'))
    print ('alpha:{}  learning rate:{}'.format(args.alpha, args.learningrate))
    ret = train.main(args)  # 訓練を実行
    return 1 - float(ret)  # lossに変換

def get_points_from_json(path):
    """ 指定パスのjsonから初期化点を取り出す """
    try:
        with open('{}/args.json'.format(path)) as f:
            arg = json.load(f)
        with open('{}/log.json'.format(path)) as f:
            log = json.load(f)
        if len(log['epoch']) != 10000:  # 学習が終わってない物は含まない
            warnings.warn('{}\'s log length is short!'.format(path))
            return None, None
        return [arg['alpha'], arg['learningrate']], log['test_variance'][-1]
    except FileNotFoundError as e:
        print (str(e))

""" 初期化 """
# ベイズ最適化のハイパーパラメータ設定
alpha_b = (0.0, 1.0)  # 入力の範囲
learningrate_b = (0.1**10, 0.001)
bounds = [alpha_b, learningrate_b]

# ロード
res = None
if args.init_points == None:  # ロード無し
    try:
        if args.n_calls < 1:  # 結果が出せない
            raise ValueError('have no samples and innadequate args.n_calls!')
        x0, y0 = None, None
        n_calls = args.n_calls
    except ValueError as e:
        print (str(e))
else:  # ロード有り
    if 'res.gz' in args.init_points:  # skopt.dumpで保存したデータをロード
        res = skopt.load(args.init_points)
        bounds = res.space.bounds  # 上書きロード
        x0, y0 = res.x_iters, res.func_vals  # サンプルデータをロード
    elif 'gp_res.pickle' in args.init_points:  # pickle.dumpで保存したデータをロード
        with open(args.init_points, 'rb') as f:
            res = pickle.load(f)
        bounds = res.space.bounds
        x0, y0 = res.x_iters, res.func_vals
    else:  # ディレクトリからサンプルデータをロード
        ll = os.listdir(args.init_points)
        ll = sorted([l for l in ll if '_' in l and not 'cache' in l])
        if len(ll) < 1:  # 長さが0
            x0, y0 = None, None
        else:
            if len(ll) == 1:  # 長さが1
                x0, y0 = get_points_from_json(
                    '{}/{}'.format(args.init_points, ll[0]))
                n_calls = abs(args.n_calls - len(x0))
            else:  # 長さが2以上
                x0, y0 = [], []
                for l in ll:
                    x0i, y0i = get_points_from_json(
                        '{}/{}'.format(args.init_points, l))
                    if x0i == None:continue
                    x0.append(x0i), y0.append(y0i)
    n_calls = args.n_calls - len(x0)

# set n_random_starts
if y0 == None:
    n_random_starts = args.n_random_starts
elif len(y0) < 5:
    n_random_starts = args.n_random_starts - len(y0)
else:
    n_random_starts = 0

""" 最適化の実行 """
temp_args = [
    func,  # the function to minimize
    bounds  # the bounds on each dimension of x
]
temp_kwargs = {
    'acq_func':'EI',  # the acquisition function n
    'n_calls':n_calls,  # the number of evaluations of f
    'n_random_starts':n_random_starts,
    # the number of random init points
    'noise':0.1**2,  # the noise level (optional)
    'random_state':0,  # the random seed
    'x0':x0,  # init points
    'y0':y0
}

if res == None and n_calls > 0:  # 条件分岐
    res = gp_minimize(*temp_args, **temp_kwargs)
elif res == None and n_calls <= 0:
    try:
        raise ValueError('have samples but innadequate args.n_calls!' \
                         '\nExpected: n_calls > 0' \
                         '\nActual: n_calls <=0')
    except ValueError as e:
        traceback.print_exc()
        quit()
elif res != None and n_calls > 0:
    res = gp_minimize(*temp_args, **temp_kwargs)
elif res != None and n_calls <= 0:
    pass

""" 結果の保存 """
skopt.dump(res, '{}/res.gz'.format(output))

""" プロット """
plt.BayesianPlot(res, output)
