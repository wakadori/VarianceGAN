#coding:utf-8
"""
DNNモデルを選択する
"""
import chainer
import sys

class SelectModule(object):
    """
    選択したDNNモデルのモジュールを返す．
    (作った理由：Chainerのバージョン1.11.0と4.2を使い分けるため)
    [Usage]
      1. 初期化．この時に選択したいDNNモデルの条件を指定．
        $ selectmodule = SelectModule(args, ...)
      2. コールでモジュールが返される．
        $ module = selectmodule()
    """
    def __init__(self, model='DCGAN', layer=5):
        """
        モジュールを選択するための条件を格納して初期化．
        [input]
          model (str)       : モデル名の指定
          layer (int or str): モデルの層数
        """
        self.model = 'DCGAN'
        self.layer = layer if isinstance(layer, int) else int(layer)
        
        self.ch_ver = chainer.__version__.split('.')[0]  # chainerバージョン
        if self.ch_ver == '4':
            chainer.global_config.cudnn_deterministic = True

    def __call__(self):
        """
        モジュールを返す．
        [output]
          module (module): DNNモデルのモジュール
        """
        path = './net/{}/{}lay/'.format(self.model, self.layer)
        import importlib
        sys.path.append(path)
        self.module = importlib.import_module('ch{}'.format(self.ch_ver))
        return self.module
