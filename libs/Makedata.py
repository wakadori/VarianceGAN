# -*- coding:utf-8 -*-
"""
自分のデータセットを作るプログラム
(chainer.dataset.DatasetMixinを継承して作り直したMakedata.py)
"""
import json
import numpy as np
import os
import sys
from chainer import dataset
from chainer.datasets import tuple_dataset
from PIL import Image


class Dataset(dataset.DatasetMixin):
    """訓練データをデータセットに変換する"""
    def __init__(self, path):
        """初期化を行う
        [input]
        -path      (char)        : 訓練データのあるディレクトリへの相対パス
        [インスタンス変数]
        -self.path (char)        : 訓練データのあるディレクトリへの相対パス
        -self.paths(list of char): 訓練データひとつずつへの相対パス
        """
        self.path = path  # 引数の格納とシャッフル
        self.paths = list()
        for name in os.listdir(path):
            self.paths.append('{}/{}'.format(path, name))
        self.shuffle()

    def __len__(self):
        """データセットの長さを返す"""
        return len(self.paths)

    def get_example(self, i):
        """データセットの要素一つを相対パスとして返す(スライスできる)"""
        return self.paths[i]

    def get_dataset(self, data, train_len=0.9):
        """データセットをCNNの形式に変換する
        [input]
        -data      (list of ***) : データセットを格納したリスト
        -train_len (float)       :  訓練用にする割合(初期値0.9)
        [output]
        -train     (TupleDataset): 訓練用データセット
        -test      (TupleDataset): テスト用データセット
        """
        threshold = int(len(data) * train_len)  # 訓練用とテスト用に分ける
        train = tuple_dataset.TupleDataset(data[0:threshold])
        test = tuple_dataset.TupleDataset(data[threshold:])
        return train, test

    def shuffle(self):
        """データセットの順番をシャッフルする"""
        self.paths = np.random.permutation(self.paths)


class ImageDataset(Dataset):
    """画像データをCNNの入力形式に変換する
    インスタンスを作成してからget_dataset()によりデータセットが得られる"""
    def __init__(self, path, size=(96, 96)):
        """初期化を行う
        [input]
        -path        (char)        : 訓練データのあるディレクトリへの相対パス
        -size        (tuple of int): リサイズするピクセルサイズ(縦，横)．デフォルト96*96
        [インスタンス変数]
        -self.size   (tuple of int): リサイズするピクセルサイズ(縦，横)
        -self.iscolor(bool)        : 画像データがカラー(modeがRGB)ならTrue
        """
        super(ImageDataset, self).__init__(path)  # スーパークラスのinit再利用
        self.size = size
        mode = Image.open(self.paths[0]).mode     # カラーモード設定
        if mode == 'RGB':
            self.iscolor = True
        elif mode == 'L':
            self.iscolor = False
        else:
            print('This data type is can\'t use')
            quit()

    def get_example(self, i):
        """データセットの要素一つを画像として返す(スライスできる)"""
        return Image.open(self.paths[i])

    def get_dataset(self):
        """画像データセットをCNNの形式に変換して返す
        [output]
        -train     (TupleDataset): 訓練用データセット
        -test      (TupleDataset): テスト用データセット"""
        data = list()  # 各データをnumpy arrayに変換する
        for i in range(len(self.paths)):
            data.append(self.image2array(self.get_example(i)))
        return super(ImageDataset, self).get_dataset(data)  # 訓練用とテスト用に分ける

    def image2array(self, image):
        """画像データをnumpy arrayに変換する"""
        image = np.asarray(image.resize(self.size), dtype=np.float32)
        if self.iscolor is True:
            image = image.transpose(2, 0, 1)/255
        else:
            image = image/255
        return image


def main():
    #data = [Dataset(sys.argv[1]), ImageDataset(sys.argv[1])]
    data = Dataset(sys.argv[1])
    return data


if __name__ == '__main__':
    data = main()
