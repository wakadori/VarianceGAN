#!/usr/bin/python
# -*- coding:utf-8 -*-
"""
DCGAN(グレースケール版)
Genをエンハンスしたい第三弾

       <Disc>
入力層 24x24x1
中間層 bn32
       12x12x32
出力層 2

       <Gen>
入力層 2
中間層 1x1x256
       bn256
       3x3x128
       bn128
       6x6x64
       bn64
       12x12x32
       bn32
出力層 24x24x1
"""
import chainer
import chainer.functions as F
import chainer.links as L

class Generator(chainer.Chain):
    def __init__(self, nz):
        super(Generator, self).__init__(
            in_linear  = L.Linear(nz, 1*1*256),
            deconv0    = L.Deconvolution2D(256,128, 5, stride=2, pad=1),
            deconv1    = L.Deconvolution2D(128, 64, 4, stride=2, pad=1),
            deconv2    = L.Deconvolution2D( 64, 32, 4, stride=2, pad=1),
            deconv_out = L.Deconvolution2D( 32,  1, 4, stride=2, pad=1),
            bn_0   = L.BatchNormalization(256),
            bn_1   = L.BatchNormalization(128),
            bn_2   = L.BatchNormalization( 64),
            bn_out = L.BatchNormalization( 32),
        )
    def __call__(self, z):
        batchsize = z.data.shape[0];
        h = F.relu(self.bn_0(F.reshape(self.in_linear(z), (batchsize, 256, 1, 1))))
        h = F.relu(self.bn_1(self.deconv0(h)))
        h = F.relu(self.bn_2(self.deconv1(h)))
        h = F.relu(self.bn_out(self.deconv2(h)))
        h = self.deconv_out(h)
        return h

class Discriminator(chainer.Chain):
    def __init__(self):
        super(Discriminator, self).__init__(
            in_conv = L.Convolution2D(1, 32, 4, stride=2, pad=1),
            out_linear = L.Linear(12*12*32, 2),
            bn0 = L.BatchNormalization(32),
        )
    def __call__(self, x):
        h = F.relu(self.bn0(self.in_conv(x)))
        h = self.out_linear(h)
        return h
