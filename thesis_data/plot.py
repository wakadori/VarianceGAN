#coding:utf-8
""" ベイズ最適化の結果をロードしプロットを行う """
import pandas as pd
import sys
from pdb import set_trace as tr

sys.path.append('../libs')  # handmade library
import PlotLib as plt

df = pd.read_csv('muririn.csv')  # load
plt.plot2d(df).savefig('./pred_func.png')  # plot
plt.plot2d(df).savefig('./pred_func.pdf')
