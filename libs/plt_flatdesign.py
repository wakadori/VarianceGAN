#coding:utf-8
#https://qiita.com/Hyperion13fleet/items/dee6cfb61959a7767d2b
import matplotlib as mpl
from cycler import cycler

def main():
    #カラーのサイクルを先に決定
    #c_cycle=("#3498db","#e74c3c","#1abc9c","#9b59b6","#f1c40f","#ecf0f1","#34495e","#446cb3","#d24d57","#27ae60","#663399", "#f7ca18","#bdc3c7","#2c3e50")
    #c_cycle=("#3498db","#e74c3c","#1abc9c","#9b59b6","#f1c40f","ACAFAF","#34495e","#446cb3","#d24d57","#27ae60","#663399", "#f7ca18","#bdc3c7","#2c3e50")  # 白を除いた
    c_cycle=cycler('color', ["#3498db","#e74c3c","#1abc9c","#9b59b6","#f1c40f","ACAFAF","#34495e","#446cb3","#d24d57","#27ae60","#663399", "#f7ca18","#bdc3c7","#2c3e50"])  # 白を除いた

    #以下がフラットデザインっぽい初期設定

    #今回は、フリーフォントを使用
    #以下のサイトからフォントをダウンロードして、読み込み
    #http://mplus-fonts.osdn.jp/mplus-outline-fonts/
    #mpl.rc('font', family='VL Gothic', size=25)
    #線のサイズや色を決定
    mpl.rc('lines', linewidth=2,color="#2c3e50")
    mpl.rc('patch', linewidth=0,facecolor="none",edgecolor="none")
    mpl.rc('text', color='#2c3e50')
    #mpl.rc('axes', facecolor='none',edgecolor="black",titlesize=24,labelsize=18,color_cycle=c_cycle,grid=False)  #
    mpl.rc('axes', facecolor='none',edgecolor="black",titlesize=24,labelsize=18,prop_cycle=c_cycle,grid=False)  # 上行のwarning回避
    #mpl.rc('axes', facecolor='none',edgecolor="none",titlesize=30,labelsize=15,prop_cycle=c_cycle,grid=False)
    mpl.rc('xtick.major',size=15,width=0)
    mpl.rc('ytick.major',size=15,width=0)
    mpl.rc('xtick.minor',size=15,width=0)
    mpl.rc('ytick.minor',size=15,width=0)
    mpl.rc('ytick',direction="out")
    mpl.rc('grid',color='#c0392b',alpha=0.3,linewidth=1)
    #mpl.rc('legend',numpoints=3,fontsize=15,borderpad=0,markerscale=3,labelspacing=0.2,frameon=False,handlelength=1,handleheight=0.5)
    mpl.rc('legend',numpoints=3,fontsize=18,borderpad=0.5,markerscale=3,labelspacing=0.2,frameon=True,framealpha=0.6,handlelength=3+1,handleheight=1.5)
    mpl.rc('figure',figsize=(10,6),dpi=80,facecolor="none",edgecolor="none")
    mpl.rc('savefig',dpi=100,facecolor="none",edgecolor="none")
