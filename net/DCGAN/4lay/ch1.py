#coding:utf-8
# http://qiita.com/mattya/items/e5bfe5e04b9d2f0bbd47
""" 4層DCGAN """
import chainer
import chainer.functions as F
import chainer.links as L

sys.path.append('./net')
import elu


class Generator(chainer.Chain):
    def __init__(self, nz):
        super(Generator, self).__init__(
            l0z = L.Linear(nz, 3*3*512),
            dc1 = L.Deconvolution2D(512, 256, 4, stride=2, pad=1, use_cudnn=False),
            dc2 = L.Deconvolution2D(256, 128, 4, stride=2, pad=1, use_cudnn=False),
            dc3 = L.Deconvolution2D(128, 64, 4, stride=2, pad=1, use_cudnn=False),
            dc4 = L.Deconvolution2D(64, 3, 4, stride=2, pad=1, use_cudnn=False),
            bnl0 = L.BatchNormalization(3*3*512),
            bndc1 = L.BatchNormalization(256),
            bndc2 = L.BatchNormalization(128),
            bndc3 = L.BatchNormalization(64),
        )
        
    def __call__(self, z, test=False):
        h = F.reshape(F.relu(self.bnl0(self.l0z(z), test=test), use_cudnn=False),
                      (z.data.shape[0], 512, 3, 3))
        h = F.relu(self.bndc1(self.dc1(h), test=test), use_cudnn=False)
        h = F.relu(self.bndc2(self.dc2(h), test=test), use_cudnn=False)
        h = F.relu(self.bndc3(self.dc3(h), test=test), use_cudnn=False)
        x = F.tanh(self.dc4(h), use_cudnn=False)
        return x
    

class Discriminator(chainer.Chain):
    def __init__(self):
        super(Discriminator, self).__init__(
            c0 = L.Convolution2D(3, 64, 4, stride=2, pad=1, use_cudnn=False),
            c1 = L.Convolution2D(64, 128, 4, stride=2, pad=1, use_cudnn=False),
            c2 = L.Convolution2D(128, 256, 4, stride=2, pad=1, use_cudnn=False),
            c3 = L.Convolution2D(256, 512, 4, stride=2, pad=1, use_cudnn=False),
            l4l = L.Linear(3*3*512, 2),
            bnc0 = L.BatchNormalization(64),
            bnc1 = L.BatchNormalization(128),
            bnc2 = L.BatchNormalization(256),
            bnc3 = L.BatchNormalization(512),
        )
        
    def __call__(self, x, test=False):
        h = elu.elu(self.c0(x))
        h = elu.elu(self.bnc1(self.c1(h), test=test))
        h = elu.elu(self.bnc2(self.c2(h), test=test))
        h = elu.elu(self.bnc3(self.c3(h), test=test))
        l = self.l4l(h)
        return l
