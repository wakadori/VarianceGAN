# -*- coding:utf-8 -*-
"""
分散(Variance)計算を行うモジュール
参考 https://groups.google.com/forum/#!msg/chainer-jp/iOHmM4m4QC0/d_GFaUTxAwAJ
"""
import autograd
import autograd.numpy as anp
#anp.numpy_boxes.ArrayBox.__repr__ = lambda self: str(self._value)
# monkey patch : https://github.com/HIPS/autograd/issues/355
import chainer
import cupy as cp
import math
import numpy as np
import theano
import theano.tensor as T
from chainer import Variable, function_node, backends
from chainer.cuda import get_array_module as armod

from pdb import set_trace as tr  # trace()  # デバッガ


class GenerationVariance(function_node.FunctionNode):
    """
    分散計算を行う．現状は単純な足し算．

    [instance variable]
     var (Variable): 分散項
     var_origin (Variable): 分散
    """

    def _var(self, *args, **kwargs):  # 分散計算手法の切り替え用
        return self._var_theano(*args, **kwargs)

    def _var_diff(self, x_gen, xp=np):
        """
        diffの微分用実装
        [input]
         - x_gen  (array of np or cp): 生成画像
         - xp     (np or cp): GPUを使うならcp
        """

        x = (x_gen + 1) / 2  # 値域を[-1,1]から[0,1]に変形
        N, C, H, W = x.shape
        # (2) 差分行列D
        D = xp.stack((x,)*N)  # Dを作成
        D = xp.abs(D.transpose((1, 0, 2, 3, 4)) - D)  # Dの転置差分
        D = xp.power(D, 2).sum(axis=(2, 3, 4))  # 2乗した和
        #D = xp.sqrt(D / (C*H*W))  # 正規化
        D = D / (C*H*W)  # 正規化
        # (3) Dの要素平均D_avr
        D_avr = D.sum() / D.size
        var = D_avr
        return var

    def _var_theano(self, x_gen, xp=np, calc_grad=False):
        """ 分散計算．theanoの自動微分を使用． """
        x_gen = (x_gen + 1) / 2  # [-1,1] -> [0,1]
        N, C, H, W = x_gen.shape

        # 関数の定義
        x = T.ftensor4()  # float32の4次元テンソル
        y = T.sum(  # (3)
            T.sum(  # (2)
                T.power(T.abs_(
                    (T.stack([x,]*N))
                    - (T.stack([x,]*N)).transpose([1, 0, 2, 3, 4])), 2),
                axis=(2,3,4)
            ) / (C*H*W)
        ) / (N*N)

        if calc_grad:  # backward用
            dy = T.grad(y, x)
            dyx = theano.function([x], dy)
            if xp == cp:
                x_gen = chainer.cuda.to_cpu(x_gen)
                grad = chainer.cuda.to_gpu(dyx(x_gen))
            else:
                grad = dyx(x_gen)
            return xp.array(grad, dtype=np.float32)  # 出力
        else:
            yx = theano.function([x], y)  # forward用
            if xp == cp:
                x_gen = chainer.cuda.to_cpu(x_gen)
                var = chainer.cuda.to_gpu(yx(x_gen))
            else:
                var = yx(x_gen)
            return xp.array(var, dtype=np.float32)  # 出力

    def forward_cpu(self, inputs):  # inputs = [x_gen]
        self.retain_inputs((0,))  # backward用に保存
        return self._var(*inputs, xp=anp),

    def forward_gpu(self, inputs):  # inputs = [x_gen]
        self.retain_inputs((0,))  # backward用に保存
        return self._var(*inputs, xp=cp),

    def backward(self, indexes, grad_outputs):
        x_gen, = self.get_retained_inputs()  # forward時の入力を取得
        gy, = grad_outputs
        xp = chainer.cuda.get_array_module(gy)
        ret = []

        grad = self._var_theano(x_gen.data, xp=xp, calc_grad=True)

        # backward用の値を返す
        gx0 = Variable(gy.data * grad)

        if 0 in indexes:
            ret.append(gx0)
        if 1 in indexes:
            ret.append(-gx0)
        return ret


def generation_variance(x0):
    return GenerationVariance().apply((x0,))[0]
