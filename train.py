#coding:utf-8
#!/usr/bin/env python
""" GANの訓練プログラム """

# support for Python2.x
from __future__ import print_function
from __future__ import absolute_import
from __future__ import generators
from __future__ import division

# chainer関連
#import chainer  # for MNIST
import chainer.functions as F
import chainer.iterators as I
from chainer import Variable, optimizers, serializers, cuda

# メインモジュール
import argparse
import ast
import json
import matplotlib
import numpy as np
import os
import pickle
import random
import sys

from datetime import datetime
from pdb import set_trace as tr  # tr()でデバッガ起動

if not 'Agg' in matplotlib.get_backend():
    matplotlib.use('Agg')  # sshで使うときのエラー回避
import matplotlib.pylab as plt

# 自作ライブラリimport
sys.path.append('./libs')
import Calcloss as cl  # loss計算
import ImageGenerator as ig  # 出力データ変換:chainer形式 -> 画像形式
import PlotLib as mplt  # lossなどのプロット
import Makedata as mk  # 入力データ変換:画像形式 -> chainer形式
import passtime as pt  # 学習時間計測
import plt_flatdesign  # matplotlibをフラットデザインにする
plt_flatdesign.main()
import SelectModule as sm  # DNNモデルを選択する

if __name__ == '__main__':
    """コマンドライン引数読み込み"""
    parser = argparse.ArgumentParser(description='GAN')
    parser.add_argument('--dataPath', default='../input/kantoku_R',
                        help='データセットディレクトリ')
    parser.add_argument('--epochsize', '-e', default=10000, type=int, help='学習回数')
    parser.add_argument('--saveTimes', '-t', default=100, type=int, help='保存回数')
    parser.add_argument('--alpha', '-a', default=0., type=float,
                        help='分散項の最大値．デフォルトは0(すなわち分散項無し)．')
    parser.add_argument('--learningrate', '-r', default=0.0001, type=float, help='学習率')
    parser.add_argument('--batchsize', '-b', default=10, type=int, help='バッチサイズ')
    parser.add_argument('--dimz', '-nz', default=100, type=int, help='zの次元数')
    parser.add_argument('--savePath', default=None,
                        help='既存のディレクトリを指定して続きを学習する時に使う．')
    parser.add_argument('--gpu', '-g', default=0, type=int,
                        help='GPU ID (負の値ならCPUモード)')
    parser.add_argument('--seed', '-s', default=None, type=int,
                        help='SEED指定用の引数．Noneは指定なし．')
    parser.add_argument('--netparams', default="{'model':'DCGAN', 'layer':5}",
                        help='使うモデルを指定する．dict形式．')
    args = parser.parse_args()


def main(args):
    """
    GANの学習を実行する．
    [input]
     args (argparse.Namespace): 条件指定．
    [output]
     test_var.data (float): 最終epochでの分散値．ベイズ最適化に使う．
    """
    """ シード設定 """
    if args.seed is None:  # シード指定なし->シード生成
        args.seed = random.randint(0, 100000000)
    else:  # シード指定あり->そのシードを使う
        pass

    random.seed(args.seed)  # シード固定
    np.random.seed(args.seed)
    cuda.cupy.random.seed(args.seed)

    print ('seed: {}'.format(args.seed))  # 表示

    """ 学習データの作成 """
    # 自前データ
    size = (48, 48)
    imagedataset = mk.ImageDataset(args.dataPath, size)
    train, test = imagedataset.get_dataset()
    iscolor = imagedataset.iscolor

    print ('Dataset: \"{}\"'.format(args.dataPath))  # 表示

    """ 学習前の準備 """
    # ネットワークモデルimport
    params = ast.literal_eval(args.netparams)
    net = sm.SelectModule(params)()

    # log name
    logname = 'log_epoch,' \
              'test_gen_loss,test_disc_loss,test_variance,test_accuracy,' \
              'train_gen_loss,train_disc_loss,train_variance.txt'

    # 全体の保存ディレクトリ作成
    output = './output'  # 名前は固定
    if os.path.isdir(output) is False:
        os.mkdir(output)

    isNone = True if args.savePath is None else False  # 指定ありフラグ
    Doexist = False  # 存在するフラグ
    if not isNone:
        Doexist = os.path.exists(args.savePath)

    if Doexist:  # 指定ありand既存 -> 学習再開
        model = os.listdir('{}/model/'.format(args.savePath))
        params = [x for x in model if 'gen' in x]
        key = lambda x:int(x.rstrip('.model.state').lstrip('gen_e'))
        start = int(key(max(params, key=key)))  # 学習開始epoch取得

    else:  # 新規 -> 新規に学習開始
        if isNone:  # 指定なし->デフォルト名
            date = datetime.now().strftime('%m%d')  # 日付
            dataPath_split = args.dataPath.split('/')  # データ名
            while dataPath_split[-1] == '':  # 末尾の空文字を除去
                dataPath_split.pop()
            args.savePath = '{}/{}{}__var{}'.format(output, date,
                dataPath_split[-1], str(args.alpha).replace('.', '_'))

        else:  # 指定あり->指定された名前(そのまま)
            pass

        # 作成(重複があれば別名)
        count = 1
        if not args.savePath.split(output+'/')[-1] in os.listdir(output):  # 重複なし
            os.makedirs(args.savePath)  # 再帰的なos.mkdir
        else:  # 重複あり
            while args.savePath.split(output+'/')[-1] + '__{}'.format(
                    count) in os.listdir(output):
                count += 1
            else:
                args.savePath = args.savePath + '__{}'.format(count)
                os.makedirs(args.savePath)  # 再帰的なos.mkdir

        os.makedirs(args.savePath + '/model/')  # モデル用
        os.makedirs(args.savePath + '/image/')  # 生成画像用

        # ログ初期化
        with open('{}/{}'.format(args.savePath, logname), 'w') as f:
            f.write('')
        start = 0  # 0epochから開始

    print ('Save directory: {}'.format(args.savePath))  # 保存ディレクトリ表示

    # 引数保存(重複があれば別名で保存)
    name = 'args'
    count = 1  # pickleで保存
    if not '{}.pickle'.format(name) in os.listdir(args.savePath):  # 重複なし
        with open('{}/{}.pickle'.format(args.savePath, name), 'wb') as f:
            pickle.dump(args, f)
    else:  # 重複あり
        while '{}{}.pickle'.format(name, str(count)) in os.listdir(args.savePath):
            count += 1
        else:
            fname = '{}/{}{}.pickle'.format(args.savePath, name, str(count))
            with open(fname, 'wb') as f:
                pickle.dump(args, f)

    count = 1  # jsonで保存
    if not '{}.json'.format(name) in os.listdir(args.savePath):  # 重複なし
        with open('{}/{}.json'.format(args.savePath, name), 'w') as f:
            json.dump(vars(args), f, indent=4, sort_keys=True)
    else:  # 重複あり
        while '{}{}.json'.format(name, str(count)) in os.listdir(args.savePath):
            count += 1
        else:
            fname = '{}/{}{}.json'.format(args.savePath, name, str(count))
            with open(fname, 'w') as f:
                json.dump(vars(args), f, indent=4, sort_keys=True)

    # ログ保存用モジュール初期化
    mplt_ = mplt.LogPlot('loss', args.savePath, logname)

    # 図作成
    fgen = ig.FigureGenerator(net, args.dimz, args.gpu, iscolor)

    """ ネットワークの作成 """
    model_gen = net.Generator(args.dimz)
    model_disc = net.Discriminator()

    if args.gpu >= 0:  # GPUを使用する設定
        cuda.get_device(args.gpu).use()
        model_gen.to_gpu()
        model_disc.to_gpu()
    xp = np if args.gpu < 0 else cuda.cupy

    # 最適化アルゴリズム設定
    optimizer_gen = optimizers.Adam(alpha=args.learningrate, beta1=0.9)
    optimizer_disc = optimizers.Adam(alpha=args.learningrate, beta1=0.9)
    optimizer_gen.setup(model_gen)
    optimizer_disc.setup(model_disc)

    if Doexist:  # パラメータのロード設定
        path = '{}/model/'.format(args.savePath)
        serializers.load_npz('{}gen_e{}.model'.format(path, start), model_gen)
        serializers.load_npz('{}gen_e{}.state'.format(path, start), optimizer_gen)
        serializers.load_npz('{}disc_e{}.model'.format(path, start), model_disc)
        serializers.load_npz('{}disc_e{}.state'.format(path, start), optimizer_disc)


    def conv(batch, color):
        """
        batchをVariableに変換する(tupleDataset[データ，ラベル]からデータのみを取り出している)
        [input]
        -batch(list of tuples):学習ループの中で使うミニバッチ
        -color(boolean):データがカラーならTrue、グレースケールならFalse
        [output]
        -x(Variable):データ本体
        """
        x = []
        if color:  # カラーの場合
            for i in range(len(batch)):
                x.append(batch[i][0])
        else:  # グレースケールの場合
            for i in range(len(batch)):
                x.append(batch[i][0])
        return Variable(xp.array(x))

    """ 学習 """
    savestep = args.epochsize/args.saveTimes  # 保存間隔
    clock = pt.passtime(args.savePath)  # 時間計測
    for epoch in range(start + 1, start + args.epochsize + 1):
        """ 訓練フェーズ """
        for i in I.SerialIterator(train, args.batchsize, repeat=False):
            closs = cl.Calcloss(alpha=args.alpha, gpu=args.gpu)  # loss初期化
            """ Generatorを訓練 """
            model_gen.zerograds()             # forward
            z = Variable(
                xp.random.uniform(-1, 1, (len(i), args.dimz), dtype=np.float32))
            x_gen = model_gen(z)
            y_gen = model_disc(x_gen)
            train_var = closs.calc_var(x_gen)  # 分散計算
            loss_gen = closs.calc_gen(y_gen)   # 生成器loss計算
            loss_gen.backward()               # backward
            optimizer_gen.update()

            """ Discriminatorを訓練 """
            model_disc.zerograds()            # forward
            x_data = conv(i, iscolor)
            y_data = model_disc(x_data)
            loss_disc = closs.calc_disc(y_gen, y_data)  # 判別器loss計算
            loss_disc.backward()              # backward
            optimizer_disc.update()
            del closs  # ロス解放

        """ テストフェーズ """
        i = next(I.SerialIterator(test, args.batchsize))
        z = Variable(                         # forward
            xp.random.uniform(-1, 1, (len(i), args.dimz), dtype=np.float32))
        x_gen = model_gen(z)
        y_gen = model_disc(x_gen)
        x_data = conv(i, iscolor)             # forward
        y_data = model_disc(x_data)

        test_closs = cl.Calcloss(alpha=args.alpha, gpu=args.gpu)  # loss計算
        test_loss_gen, test_loss_disc, test_var = test_closs(y_gen, y_data, x_gen)
        y = np.concatenate((cuda.to_cpu(y_data.data),  # 正解率を求める
                            cuda.to_cpu(y_gen.data)))
        t = np.asarray([0, ]*len(i)+[1, ]*len(i), dtype=np.int32)
        test_accuracy = F.accuracy(y, t)
        del test_closs  # ロス解放

        """ 情報表示 """
        log = [epoch, test_loss_gen.data, test_loss_disc.data,
               test_var.data, test_accuracy.data,
               loss_gen.data, loss_disc.data, train_var.data]

        mess = ''
        mess += 'epoch{} | '.format(log[0])
        mess += 'test gen:{} '.format(log[1])
        mess += 'disc:{} '.format(log[2])
        mess += 'var:{} '.format(log[3])
        mess += 'acc:{} | '.format(log[4])
        mess += 'train gen:{} '.format(log[5])
        mess += 'disc:{} '.format(log[6])
        mess += 'var:{}'.format(log[7])
        print(mess)

        """ 保存 """
        logtxt = ''  # ログ保存
        for l in log:
            logtxt += '{},'.format(l)
        logtxt = logtxt[:-1]
        with open('{}/{}'.format(args.savePath, logname), 'a') as f:
            f.write(logtxt + '\n')
        del log  # ログ解放
        if ((epoch - start) % savestep) == 0:
            print()
            # 時間測定と保存
            clock.display(inf='epoch {}'.format(epoch))
            print('saving model...')

            # モデル保存
            serializers.save_npz('{}/model/gen_e{}.model'
                                 .format(args.savePath, epoch), model_gen)
            serializers.save_npz('./{}/model/gen_e{}.state'
                                 .format(args.savePath, epoch), optimizer_gen)
            serializers.save_npz('{}/model/disc_e{}.model'
                                 .format(args.savePath, epoch), model_disc)
            serializers.save_npz('./{}/model/disc_e{}.state'
                                 .format(args.savePath, epoch), optimizer_disc)

            # 生成画像保存(現在のepoch)
            param = '{}/model/gen_e{}.model'.format(args.savePath, epoch)
            savename = '{}/image/image_e{}'.format(args.savePath, epoch)
            if iscolor is False:
                plt.style.use('grayscale')
            fig = fgen(param, rows=1, shape=[10, 10])  # プロットと保存
            fig.savefig(savename)
            del fig
            if iscolor is False:
                plt.style.use('default')

            # 生成画像図保存(これまでのepoch)
            params = [x for x in os.listdir('{}/model/'.format(  # 保存済みの生成用モデルを
                args.savePath)) if ('.model' in x) and ('gen' in x)]
            params = sorted(params,                              # epoch昇順ソートする
                            key=lambda x: int(x.split('.')[-2].split('_e')[-1]))
            if len(params) >= 10:                                # プロットする10個を取り出す
                params = ['{}/model/{}'.format(args.savePath, params[x-1]) for x in range(
                    len(params)//10, len(params)+1, len(params)//10)]
                savename = '{}/images.png'.format(args.savePath)
                if iscolor is False:
                    plt.style.use('grayscale')
                fig = fgen(params, rows=2, shape=[5, 5])  # プロットと保存
                fig.savefig(savename)
                del fig
                if iscolor is False:
                    plt.style.use('default')

            # lossグラフプロットと保存
            mplt_()
            plt.close()

    return test_var.data  # ベイズ最適化用の値を返す

if __name__ == '__main__':
    test_var = main(args)
