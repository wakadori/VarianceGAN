#coding:utf-8
""" データからグラフを作成 """
import json
import linecache
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plt_flatdesign  # グラフデザイン設定
plt_flatdesign.main()

from pdb import set_trace as tr
from skopt.plots import plot_convergence

class LogPlot(object):
    """
    logのtxtからlossのグラフなどを作成．
    初期化時にインスタンスを作成し保存時に呼び出す．
    """
    def __init__(self, savename, savePath, logname):
        """
        学習の条件などを渡す．
        [input]
         savename (str): 保存名
         savePath (str): 保存ディレクトリ
         logname (str): log txtファイルの名前
        """
        self.savename = savename
        self.savePath = savePath
        self.logname = logname

        self.ptr = 1  # 前回読んだlogのポインタ

        self.log = {}
        self.lognames = logname.rstrip('.txt').lstrip('log_').split(',')
        for i in self.lognames:
            self.log[i] = list()

    def __call__(self):
        """ グラフの作成と保存を実行． """
        path = '{}/{}'.format(self.savePath, self.logname)  # log読み込み
        num_lines = sum(1 for l in open(path))  # 行数取得
        # https://stackoverflow.com/questions/845058/how-to-get-line-count-cheaply-in-python

        for l in range(self.ptr, num_lines + 1):
            line = linecache.getline(path, l)  # 新しい部分を1行ずつ読込
            values = [float(v) for v in line.rstrip('\n').split(',')]
            linecache.clearcache()
            for key, value in zip(self.lognames, values):
                self.log[key].append(value)
            self.ptr += 1

        with open('{}/log.json'.format(self.savePath), 'w') as f:  # json保存
            json.dump(self.log, f, indent=4, sort_keys=True)

        # グラフ描画
        plt.figure(figsize = (16, 10))
        skip = len(self.log['epoch']) // 100  # x軸の間隔
        skip = 1 if skip == 0 else skip

        def plot(height, width, ptr, x, y, skip,
                 xlabel='epoch', ylabel='loss', legend='legend', title='title'):
            """複数のグラフをプロットする
            [input]
            height (int)          : グラフの縦の数
            width  (int)          : グラフの横の数
            ptr    (int)          : 現在位置．横書き順(Z順?)．
            x      (list of float): 横軸の値
            y      (list of float): 縦軸の値
            skip   (int)          : プロット点の間隔
            xlabel (str)          : 横軸ラベル
            ylabel (str)          : 縦軸ラベル
            legend (str)          : 凡例
            title  (str)          : グラフのタイトル
            """
            plt.subplot(height, width, ptr)
            plt.plot(x[::skip], y[::skip], label=legend)
            plt.legend()
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            plt.title(title)

        l = self.log
        plot(2, 2, 1, l['epoch'], l['train_gen_loss'], skip,  # 生成器loss
             legend='train', title='gen loss')
        plot(2, 2, 1, l['epoch'], l['test_gen_loss'], skip,
             legend='test', title='gen loss')
        plot(2, 2, 2, l['epoch'], l['train_variance'], skip,  # 分散
             ylabel='variance', legend='train', title='variance')
        plot(2, 2, 2, l['epoch'], l['test_variance'], skip,
             ylabel='variance', legend='test', title='variance')
        plot(2, 2, 3, l['epoch'], l['train_disc_loss'], skip,  # 判別器loss
             legend='train', title='disc loss')
        plot(2, 2, 3, l['epoch'], l['test_disc_loss'], skip,
             legend='test', title='disc loss')
        plot(2, 2, 4, l['epoch'], l['test_accuracy'], skip,  # 正解率
             ylabel='acc', legend='test', title='accuracy')

        plt.tight_layout()
        path = '{}/{}.png'.format(self.savePath, self.savename)
        plt.savefig(path, dpi=100)

def BayesianPlot(res, output):
    """
    ベイズ最適化用のプロット
    [inputs]
     res : ベイズ最適化の返り値
     output : 図の保存ディレクトリ
    """
    plt.rcParams['font.size'] = 28

    # Acquition Function
    plt.figure(figsize=(14, 9))
    plot_convergence(res)
    plt.savefig('{}/EI.png'.format(output), dpi=300)

    # 2次元平面上
    df = make_gp2df(res)
    df.to_csv('{}/pred_func.csv'.format(output))
    fig = plot2d(df)
    plt.savefig('{}/pred_func.png'.format(output), dpi=300)
    plt.savefig('{}/pred_func.pdf'.format(output), dpi=300)
    plt.close()

def make_gp2df(res):
    """ convert a result of bayesian optimization to pandas.DataFrame"""
    df = pd.DataFrame(  # make data for plotting
        index=range(len(res.func_vals)),
        columns=['alpha', 'learningrate', 'test_variance'])
    df[['alpha', 'learningrate']] = res.x_iters  # x (list of pair float)
    df['test_variance'] = res.func_vals  # y (list of float)
    return df

def plot2d(df):
    """ 入出力のpandas.DataFrameから2次元平面にプロット """
    fig = plt.figure(figsize=(14, 9))  # initialize
    ax = fig.add_subplot(111)

    df_sort = df.sort_values('test_variance', ascending=False)  # descending order sorting
    dfm = df_sort.iloc[0, :]  # maximum row
    ax.set_title(
        r'$x_0^* = {:.1e}, x_1^* = {:.1e}, func(x_0^*, x_1^*) = {:.1e}$'.format(
            *list(dfm)
        ), fontsize=20
    )

    ax.set_xlabel(r'$x_0:\alpha$', fontsize=32)
    ax.set_ylabel(r'$x_1:learning rate$', fontsize=32)
    ax.ticklabel_format(style="sci",  axis="x",scilimits=(0,0))  # 指数表記
    ax.ticklabel_format(style="sci",  axis="y",scilimits=(0,0))

    margin = 0.1  # set margin of inputs
    x, y = ([0.0, 1.0], [0.1**100, 0.1**3])
    m_x = margin * (x[1] - x[0])
    m_y = margin * (y[1] - x[0])
    ax.set_xlim(x[0] - m_x, x[1] + m_x)
    ax.set_ylim(y[0] - m_y, y[1] + m_y)
    m = 1/(10*(1+margin))
    ax.axvspan(0, 1, ymin=m, ymax=1-m, facecolor='b', alpha=0.1)  # draw background rectangle

    sc = plt.scatter(  # plot
        df.alpha,  # x0
        df.learningrate, # x1
        s=300,
        c=df.test_variance,  # y
        cmap='Greys', edgecolors='black', linewidths=1)

    cbar = plt.colorbar(sc)
    cbar.ax.ticklabel_format(axis='x', style="sci", scilimits=(-1,-1))
    plt.plot(df.alpha, df.learningrate, linestyle='dashed', color='black', alpha=0.8)

    plt.tight_layout()
    return fig
