# -*- coding:utf-8 -*-
""" loss計算を行うモジュール """
import chainer.functions as F
import math
import numpy as np
from chainer import Variable, cuda
from pdb import set_trace as trace  # trace()  # デバッガ

import sys
sys.path.append('libs')
import GenerationVariance as gv


class Calcloss(object):
    """GANのlossを計算する．
    instance = Calcloss()で初期化を行う．
    訓練時はcalc_var，calc_gen，calc_discを訓練手順に合わせて実行することでそれ
    ぞれのlossが返される．
    テスト時はinstance.__call__()を実行する('.__call__'は省略可)ことで，生成器l
    oss，判別器loss，分散項が返される．"""
    def __init__(self, alpha=0., gpu=0):
        """
        コンストラクタ．GPU設定をする．
        [input]
            -alpha    (float)   : 分散項の最大値．初期値0
            -gpu      (int)     : 使用するGPUID．初期値0(すなわち分散項無し)
        [インスタンス変数]
            -下以外はinputと同じ
            -xp         (module)  : npとして使うGPU処理用module
            -var        (Variable): D_avrの値
        """
        self.alpha = alpha  # 係数
        self.gpu = gpu

        self.xp = np if self.gpu < 0 else cuda.cupy
        self.alpha = self.xp.array(self.alpha)
        self.var = Variable(self.xp.array(0., dtype=np.float32))

    def __call__(self, y_gen, y_data, x_gen):
        """
        lossを計算(テスト用)
        [input]
            -y_gen    (Variable): 生成画像入力時のDisc出力
            -y_data   (Variable): 訓練データ入力時のDisc出力
            -x_gen    (Variable): 生成画像
        [output]
            -loss_gen (float)   : 生成器loss
            -loss_disc(float)   : 判別器loss
            -var      (float)   : 分散項(上二つに含まれている)
        """
        var = self.calc_var(x_gen)
        loss_gen = self.calc_gen(y_gen)
        loss_disc = self.calc_disc(y_gen, y_data)
        return loss_gen, loss_disc, var

    def calc_gen(self, y_gen):
        """生成器lossを計算する
        [input]
            -y_gen    (Variable): 生成画像入力時のDisc出力
        [output]
            生成器loss(Variable)"""
        loss = F.softmax_cross_entropy(y_gen, Variable(
            self.xp.zeros(len(y_gen.data), dtype=np.int32)))
        return loss

    def calc_disc(self, y_gen, y_data):
        """判別器lossを計算する
        [input]
            -y_gen    (Variable): 生成画像入力時のDisc出力
            -y_data   (Variable): 訓練データ入力時のDisc出力
        [output]
            判別器loss(Variable)"""
        loss = F.softmax_cross_entropy(y_gen, Variable(  # Gen画像入力
            self.xp.ones(len(y_gen.data), dtype=np.int32)))
        loss += F.softmax_cross_entropy(y_data, Variable(  # 訓練データ入力
            self.xp.zeros(len(y_data.data), dtype=np.int32)))

        loss = (1-self.alpha) * loss + self.alpha * (1-self.var)
        return loss

    def calc_var(self, x_gen):
        """分散項を計算してself.varに格納
        [input]
            -x_gen    (Variable): 生成画像
        [output]
            -分散値   (Variable)
        """
        self.var = gv.generation_variance(x_gen)
        return self.var
